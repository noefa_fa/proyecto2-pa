# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from Archivo import Archivo
import os


class dlgPrincipal():

    def __init__(self, titulo=""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ventanas.ui")

        self.dialogo = self.builder.get_object("ChooserDialog")
        self.dialogo.resize(600, 400)
        self.dialogo.set_title("Seleccione carpeta")

        # Boton aceptar
        boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        # Mostrar imagen
        boton_aceptar.set_always_show_image(True)
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

        # boton cancelar
        boton_cancelar = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL)
        boton_cancelar.set_always_show_image(True)
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

        self.dialogo.show_all()

    def boton_aceptar_clicked(self, btn=None):
        # traer archivo seleccionado
        self.archivo = self.dialogo.get_file()
        """
            con get_current_file() se obtiene la ruta del archivo seleccionado
            get_file() retorna un archivo giofile, y para obtener el
            nombre del archivo se utiliza get_basename()
        """

        # ubicacion del archivo
        self.ruta = self.dialogo.get_current_folder()
        self.archivos = os.listdir(self.ruta)

        self.dialogo.destroy()

    def boton_cancelar_clicked(self, btn=None):
        self.dialogo.destroy()
