# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlgPrincipal import dlgPrincipal
from dlgImagen import imagen
from Archivo import Archivo
from Combobox import Combobox
from dlgAcerca_de import dlgAcerca_de
import os


# Se genera la clase principal en este caso de la ventana
class wnPrincipal():

    def __init__(self):

        self.builder = Gtk.Builder()
        # Se obtiene el archivo
        self.builder.add_from_file("./ui/ventanas.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        # Poner un titulo bonitoo uwu
        self.window.set_title("Visualizador de archivos PDB")
        self.window.maximize()
        self.window.show_all()

        # botones de menú
        boton_abrir = self.builder.get_object("btnAbrir")
        boton_abrir.connect("clicked", self.boton_abrir_clicked)

        # botones de Visualizador de imagen
        boton_Imagen = self.builder.get_object("btnImagen")
        boton_Imagen.connect("clicked", self.boton_clicked_Imagen)

        # Botón informacion de la aplicacion
        button_about = self.builder.get_object("btnAbout")
        button_about.connect("clicked", self.btn_about_clicked)

        # Label info.
        self.info = self.builder.get_object("info")
        # No se como se hace el label
        self.label = self.builder.get_object("GtkLabel")

        # Tree
        self.tree = self.builder.get_object("GtkTree")
        self.tree_model = Gtk.ListStore(*(2 * [str]))
        self.tree.set_model(model=self.tree_model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Código proteína",
                                    cell_renderer=cell,
                                    text=0)
        self.tree.append_column(column)

        column = Gtk.TreeViewColumn(title="Header",
                                    cell_renderer=cell,
                                    text=1)
        self.tree.append_column(column)

        self.tree.connect("cursor-changed", self.preview)

        self.window.show_all()

    # Cuando se presione el boton
    def boton_abrir_clicked(self, btn=None):
        self.archivo_pdb = []
        # Ruta del archivo.pdb
        self.dlg = dlgPrincipal()
        self.dlg.dialogo.run()
        self.label.set_text(self.dlg.ruta)
        # Se sacan pdb de carpeta
        for i in self.dlg.archivos:
            if os.path.isfile(os.path.join(self.dlg.ruta, i)) and i.endswith(".pdb"):
                self.archivo_pdb.append(i)
        self.pdb = []
        # Datos para el listore
        for j in self.archivo_pdb:
            archivo_name = str(j)
            a_pdb = Archivo(archivo_name)
            self.pdb.append(a_pdb)
        # Se agrega al ListStore
        for k in self.pdb:
            self.tree_model.append([k.code, k.header])

    def preview(self, btn=None):
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        # Codigo.pdb
        archivo_name = model.get_value(it, 0) + ".pdb"
        ruta = self.dlg.ruta
        dlgImagen = imagen(ruta, archivo_name)

        self.imagen = self.builder.get_object("imagen")
        self.imagen.set_from_file(dlgImagen.image_name)
        self.imagen.show_all()

        # self.pdb tiene los pdb, self.archivo tiene los nombres
        for i in range(len(self.pdb)):
            if str(self.archivo_pdb[i]) == archivo_name:
                # Se ven los primeros 1000 caracteres
                self.info.set_text(self.pdb[i].text)

    def boton_clicked_Imagen(self, btn=None):
        # Visualiza imagen y texto sobre un archivo seleccionado.
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return

        archivo_name = model.get_value(it, 0) + ".pdb"
        self.dlgInfo = Combobox(archivo_name)
        self.dlgInfo.dialogo.run()

    # Informacion acerca de la aplicacion
    def btn_about_clicked(self, btn=None):
        self.dlgAbout = dlgAcerca_de()
        self.dlgAbout.dialog.run()
        self.dlgAbout.dialog.destroy()


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()


