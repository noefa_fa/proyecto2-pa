# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from  biopandas.pdb  import  PandasPdb


class Archivo():
    def __init__(self, archivo_name=""):

        ppdb = PandasPdb()
        ppdb.read_pdb('./archivos_pdb/' + archivo_name)
        # Para visualización
        self.text = ppdb.pdb_text[:1000]
        self.header = ppdb.header
        self.code = ppdb.code

