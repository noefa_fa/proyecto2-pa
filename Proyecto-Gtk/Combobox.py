# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from Archivo import Archivo


class Combobox():

    def __init__(self, archivo_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ventanas.ui")

        self.dialogo = self.builder.get_object("AbrePDB")
        self.dialogo.resize(500, 500)
        self.dialogo.set_title("Informacion")
        self.archivo_name = archivo_name

        # Cargar ruta del archivo.
        ppdb = PandasPdb()
        self.path = ppdb.read_pdb('./archivos_pdb/' + archivo_name)

        # Botón aceptar
        button_save = self.builder.get_object("btnGuardarInfo")
        button_save.connect("clicked", self.btn_guardar_clicked)

        # Botón cancelar.
        button_cancel = self.builder.get_object("btnCancelarInfo")
        button_cancel.connect("clicked", self.btn_cancelar_clicked)

        # Combobox
        self.combobox = self.builder.get_object("CCombobox")
        render_text = Gtk.CellRendererText()
        self.combobox.pack_start(render_text, True)
        self.combobox.add_attribute(render_text, "text", 0)
        self.modelo = Gtk.ListStore(str)

        self.combobox.connect("changed", self.combobox_changed)

        lista = ["ATOM", "HETATM", "ANISOU", "OTHERS"]
        for index in lista:
            self.modelo.append([index])
        self.combobox.set_model(self.modelo)

        # Selecciona el tipo de combobox (atom, hetatm, anisou, others)
        self.comboboxvalue = None

        # Label.
        self.data = self.builder.get_object("Proteina")

        self.dialogo.show_all()

    def combobox_changed(self, comboboxvalue=None, archivo_name=""):
        it = self.combobox.get_active_iter()
        model = self.combobox.get_model()
        self.comboboxvalue = model[it][0]
        # Generar datafragmento requerido.
        ppdb = PandasPdb()

        self.path = ppdb.read_pdb('./archivos_pdb/' + self.archivo_name)

        # Cuando el dataframe contenga información, se añade al label.
        if ppdb.df[self.comboboxvalue].empty is False:
            # Conierte el dataframe en str
            self.data.set_text(str(ppdb.df[self.comboboxvalue].to_string()))

        else:
            self.data.set_text("Sin información disponible")

    def btn_guardar_clicked(self, btn=None, ruta=""):
        ppdb = PandasPdb()
        self.path = ppdb.read_pdb('./archivos_pdb/' + self.archivo_name)
        # Para guardar archivo
        guardar = self.archivo_name + "_" + self.comboboxvalue
        # se crea el archivo
        ppdb.to_pdb(path= "./archivos_pdb/" + guardar + ".pdb",
                    records =[self.comboboxvalue],
                    gz=False, append_newline=True)
        self.dialogo.destroy()

    def btn_cancelar_clicked(self, btn=None):
        self.dialogo.destroy()
