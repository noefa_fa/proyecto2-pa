# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import pymol
import __main__
# Pymol : quiet and no GUI
__main__ . pymol_argv = ["pymol", "- qc"]
pymol.finish_launching()
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Clase que generara la imagen de la proteína
class imagen():

    def __init__(self, ruta='', archivo_name=''):
        self.ruta = ruta
        self.archivo_name = archivo_name
        self.visualizar()

    def visualizar(self):
        pdb_file = self.ruta + "/" + self.archivo_name
        pdb_name = self.archivo_name
        pymol.cmd.load(pdb_file, pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        print(pymol.cmd.get_names())
        pymol.cmd.hide('all')
        pymol.cmd.show('cartoon')
        pymol.cmd.set('ray_opaque_background', 0)
        pymol.cmd.pretty(pdb_name)
        pymol.cmd.png("%s.png" % (pdb_name))
        self.image_name = ("%s.png" % (pdb_name))
        pymol.cmd.ray()

        

