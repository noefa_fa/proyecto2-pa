# Protein Data Visualization

Es una aplicación que nos permite visualizar y tener información acerca de proteínas a traves de archivos pdb contenidos en carpetas presentes en los archivos del equipo.

# Empezando
La aplicacion nos permite seleccionar una carpeta que cargara solo archivos PDB. Al seleccionar uno de los archivos, se cargara la imagen correspondiente a la proteina y sus primero 1000 caracteres de información.
Al presionar el boton visualizar este tendra ATOM, ANISOU, HETATM y OTHERS, y nos permitira guardar en un archivo nuevo PDB con la informacion que seleccione. El boton acerca de nos dara información sobre la versión de la aplicación y creadores

# Prerequisitos
Se necesita tener instalado python3, este se puede instalar a traves de la terminal con el siguiente comando:  sudo apt-get install python3.
Se debe instalar import gi con el codigo siguiente: apt-cache search python3-gi, apt install python3-gi.
Instalar glade, que nos entregrara el soporte para contruir la parte grafica, con el siguiente codigo: apt install glade.
Instalar pymol, que nos permite descargar y visualizar imagenes, con los siguientes codigos: primero se debe colcar en la terminal apt-cache search pymol y luego sudo apt-get install pymol.
Instalar biopandas, que nos permite manipular archivos de tipo pdb, con el siguiente codigo: pip3 install biopandas.

# Instalación
Instalación de requisitos paso a paso
Abrir terminal e ingresar los siguientes comandos:
# Actualización sistema
`sudo apt-get update`
# Instalar Python3
`sudo apt-get install python3`
# Instalar Gtk
`sudo apt install python3-gi`
# Instalar Biopandas
`pip3 install biopandas`
# Ejecutar Pymol
`bash pymol.bash`
# Instalar Glade
`sudo apt-get install glade`


# Ejecutando las pruebas
Se abre la aplicación y esta posee en la parte superior 3 botones. 
En primera instancia se debe seleccionar una carpeta, y en la ventana principal debajo de los botones cargarán solo archivos PDB. Lo que no sea pdb no lo acepta la aplicación y si la carpeta seleccionada no tiene archivos PDB, se toma como vacía. 
Luego se debe seleccionar uno de los arhivocs listados y en la parte inferior de la lista se cargará una imagen y los primeros 1000 caracteres del archivo seleccionado.
Con el archivo seleccionado se debe presionar el botón visualizar, el cual dará las opciones de ver 4 apartados del archivo: ATOM, ANISOU, HETATM y OTHERS. Cuando se elija uno está la opcióón de guardar lo seleccionado mediante el boton guardar, lo que creerá un archivo pdb con el apartado a fin de dar posibilidad de crear una imagen también. Cabe destacar que si se hace click en el boton visualizar sin haber seleccionado una proteíína del listado, no sucederá nada.
El boton acerca de da información sobre la versión de la aplicación y creadores.

# Despliegue 
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Glade: Un diseñador de interfaces para GTK+ y GNOME.
- Gtk : serie de herramientas multiplataforma para crear interfaces gráficas.
- Pymol: sistema de visualización molecular
- Liberías python: biopandas, gi (de Gtk) y os. Acceder a funciones y métodos proprios de estas librerías.

# Versión
2.0

# Autores
- Noemí Gómez - nogomez19@alumnos.utalca.cl
- Josefa Hillmer - jhillmer19@alumnos.utalca.cl

# Licencia
Este proyecto esta bajo la licencia GPL.3.0



